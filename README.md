# Random Forest Analysis

This Python script conducts a comprehensive Random Forest analysis on a given dataset.

## Features

- Trains a Random Forest model on the dataset
- Evaluates the model's performance using accuracy, precision, and recall metrics
- Generates confusion matrices for the model predictions
- Extracts feature importance and reduces the feature set based on importance and redundancy
- Visualizes the data using UMAP in 2D and 3D

## Requirements

- Python 3.7 or later
- Dependencies: NumPy, Pandas, Matplotlib, Seaborn, Scikit-learn, Fastai, UMAP

## Usage

The `random_forest_analysis` function is the core of this module. It performs random forest analysis, involving data preprocessing, model training, feature importance calculation, feature selection, model evaluation, and visualization.

Here is an example of its usage:

```python
random_forest_analysis(path, df, dep_var, idx=None, n_iterations=10, bound=0.005, test_size=0.2, cv_folds=5, 
                       n_estimators=40, max_features=0.5, min_samples_leaf=5, max_card=20, 
                       distance_threshold=0.01, parallel=False, umap_op=False, n=250, verbose=False)
```

### Parameters

- `path` (str): The path to the directory where the outputs will be saved.
- `df` (pd.DataFrame): The input pandas DataFrame.
- `dep_var` (str): The name of the target variable in the DataFrame.
- `idx` (Optional[List[int]]): The indices for the train-test split. If None, a default split will be used.
- `n_iterations` (int): The number of iterations for feature importance calculation. Default is 10.
- `bound` (float): The MCC score bound for feature selection. Default is 0.005.
- `test_size` (float): The proportion of the dataset to include in the test split. Default is 0.2.
- `cv_folds` (int): The number of cross-validation folds for feature selection. Default is 5.
- `n_estimators` (int): The number of trees in the random forest. Default is 40.
- `max_features` (float): The number of features to consider when looking for the best split. Default is 0.5.
- `min_samples_leaf` (int): The minimum number of samples required to be at a leaf node. Default is 5.
- `max_card` (int): Maximum cardinality of a numerical categorical variable. Default is 20.
- `distance_threshold` (float): The distance threshold for feature clustering. Default is 0.01.
- `parallel` (bool): Whether to run feature importance calculation in parallel. Default is False.
- `umap_op` (bool): Whether to generate UMAP plots. Default is False.
- `n` (int): The number of neighbors to consider for UMAP. Default is 250.
- `verbose` (bool): Whether to print progress messages. Default is False.

### Returns

- `to_keep` (List[str]): A list of the names of the important features.
- `to_keep_final` (List[str]): A list of the names of the final features.

### Directory Structure

The `random_forest_analysis` function generates a number of directories and files during its execution. The main directory is given by the `path` argument, and within it, a directory named after the dependent variable (`dep_var`) is created. This directory contains the following:

- `metrics.log`: A log file containing various metrics and information generated during the analysis.
- `confusion_matrices`: Contains confusion matrices.
- `feature_importance`: Contains a CSV file with the feature importances and a feature importance plot.
- `UMAP`: Contains 2D and 3D UMAP plots (if UMAP operation is enabled).

## Preprocessing Your Data with `preprocessing`

Before you can train a model on your data, it needs to be properly preprocessed. This is where the `preprocessing` function comes into play. This function applies several preprocessing steps to your DataFrame, including splitting the data into training and validation sets, filling missing values, normalizing numerical features, and categorizing categorical features.

Here is an example of how you can use the `preprocessing` function:

```python
from rfa.operations import preprocessing

# Preprocess the data
xs, y, valid_xs, valid_y = preprocessing(df, dep_var)
```

In the above example, `df` is your DataFrame and `dep_var` is the dependent variable (the target for prediction). The function returns preprocessed feature sets (`xs`, `valid_xs`) and target arrays (`y`, `valid_y`) for both the training and validation sets.

After preprocessing, your data is ready to be passed to a model for training. By taking care of these preprocessing steps for you, the `preprocessing` function helps ensure that your data is in the right format and condition for effective model training.

## Calculating Feature Importances with `get_feature_importance`

After preprocessing your data, the next step is often to understand the importance of each feature in your dataset. The `get_feature_importance` function can assist you with this. It trains a random forest model and uses this model to compute the importance of each feature.

Here is an example of how to use the `get_feature_importance` function:

```python
from rfa.operations import get_feature_importance

# Compute feature importances
fi = get_feature_importance(xs, y, dep_var, n_estimators, max_features, min_samples_leaf, n_iterations, parallel, path)
```

In the above example, `xs` and `y` are your preprocessed training features and targets. The `n_estimators`, `max_features`, and `min_samples_leaf` parameters are used to control the configuration of the random forest model. The `n_iterations` parameter determines how many times to repeat the feature importance calculation for averaging, while `parallel` decides whether to perform these iterations in parallel or not. The `path` parameter, if specified, determines where to save the computed feature importances as a CSV file.

By returning a DataFrame of feature importances, the `get_feature_importance` function gives you valuable insights into your data, helping you understand which features are most influential in the model's predictions.

## Feature Reduction with the FeatureReducer Class

`FeatureReducer` is a class designed to help in reducing the number of features in a dataset based on their importance and impact on model performance. This can be particularly useful in high-dimensional datasets where overfitting might be a concern, or when computational resources are limited.

To use this class, you start by instantiating it, providing your desired maximum difference in the Matthews correlation coefficient (MCC) score (`mcc_bound`) and the number of cross-validation folds (`cv_folds`). These parameters will guide the feature reduction process.

```python
from module_name import FeatureReducer

fr = FeatureReducer(mcc_bound, cv_folds, n_estimators, max_features, min_samples_leaf)
```

The main method in this class is `reduce_features()`, which takes in your training and validation data, labels, and a DataFrame of feature importances. Optionally, you can provide an existing model to this method. If a model is not provided, a new one will be trained on all features.

```python
important_model, important_train_data, important_valid_data, kept_features = fr.reduce_features(
    train_data, train_labels, valid_data, valid_labels, feature_importances
)
```

This method iteratively removes the least important features (those with importances below the 99th percentile) and retrains the model, checking at each step whether the MCC score remains within the acceptable bounds. If it does not, the method will attempt to keep more features.

If a satisfactory reduced feature set cannot be found, the method will return the original model and dataset. If a reduced feature set is found that maintains the MCC score within the acceptable bounds, the method will return the new model trained on the reduced dataset, the reduced datasets, and the list of kept features.

## Removing Redundant Features Using `cluster_features`

Our module also provides a function to help you identify and remove redundant features from your dataset. `cluster_features` uses Spearman correlation and hierarchical clustering to detect groups of features that are highly correlated with each other. These correlated features can often be redundant, providing little additional information for your model and possibly leading to overfitting.

Here's how you can use the `cluster_features` function:

```python
from rfa.operations import cluster_features

# Get list of redundant features to drop
redundant_features = cluster_features(df, distance_threshold)
```

In the above example, `df` is your DataFrame and `distance_threshold` is the threshold for forming clusters. The function will return a list of column names that are recommended to be dropped. 

After identifying redundant features, you can easily drop them from your DataFrame:

```python
df = df.drop(columns=redundant_features)
```

By using `cluster_features`, you can simplify your dataset, potentially improving your model's performance and its ability to generalize to new data.


## Using the Module in a Modular Fashion

This module has been designed to allow users to utilize its functions in a modular manner. This means you can call individual functions to obtain intermediate outputs, providing greater flexibility and control over your data analysis process.

Here's a common workflow:

1. **Data Preprocessing:** Start with your raw data and use the `preprocessing` function to prepare it for analysis. This function will handle tasks such as splitting the data into training and validation sets, filling missing values, normalizing, and categorizing. This provides you with the processed feature sets and target arrays for both training and validation.

```python
from rfa.operations import preprocessing

xs, y, valid_xs, valid_y = preprocessing(df, max_card, dep_var)
```

2. **Feature Importance Calculation:** Once you have the preprocessed data, you can calculate feature importances using the `get_feature_importance` function. This function will train a random forest model and compute feature importances based on this model. If you wish, you can save these importances as a CSV file for later reference.

```python
from rfa.operations import get_feature_importance

fi = get_feature_importance(xs, y, dep_var, n_estimators, max_features, min_samples_leaf, n_iterations, parallel, path)
```

3. **Find Important Features:** With the preprocessed data and feature importances, you can now reduce the number of features using the `FeatureReducer` class. This class is initialized with parameters needed for the random forest model and cross-validation. After initializing, the `reduce_features` method is called with the training and validation datasets, the labels, and the feature importances. This method iteratively reduces the number of features while ensuring that the performance (measured by MCC score) does not decrease beyond an acceptable boundary.

```python
from rfa.feature_reducer import FeatureReducer

# Initialize FeatureReducer
reducer = FeatureReducer(mcc_bound, cv_folds, n_estimators, max_features, min_samples_leaf)

# Reduce features
model, important_train_data, important_valid_data, kept_features = reducer.reduce_features(
    xs,
    y,
    valid_xs,
    valid_y,
    fi,
)

# Now, 'important_train_data' and 'important_valid_data' are the reduced training and validation data,
# 'model' is the random forest model trained on the reduced data,
# and 'kept_features' is a list of the names of the kept features.
```

4. **Removing Redundant Features:** After reducing features based on their importance, we can further refine our dataset by identifying and removing redundant features. Redundancy here refers to features that are highly correlated with each other, which may not add much value to the model and can instead contribute to overfitting. We use the `cluster_features` function for this purpose. This function uses Spearman correlation and hierarchical clustering to identify clusters of highly correlated features and suggests dropping all but one feature from each cluster.

```python
from rfa.operations import cluster_features

# Get list of redundant features to drop
redundant_features = cluster_features(important_train_data, distance_threshold)

# Drop the redundant features from the reduced datasets
final_train_data = important_train_data.drop(redundant_features, axis=1)
final_valid_data = important_valid_data.drop(redundant_features, axis=1)
```

Now, `final_train_data` and `final_valid_data` are the training and validation datasets with both unimportant and redundant features removed. The model can be further trained on this refined dataset, potentially improving its performance and generalization ability.

This modular approach allows you to access and potentially modify the data at each stage of the process. For instance, you can examine the processed data before it's passed to the random forest model, or use the calculated feature importances for further data analysis or visualization tasks.

## Contributing

Contributions are welcome. Please open an issue to discuss your ideas or initiate a pull request with your changes.
```