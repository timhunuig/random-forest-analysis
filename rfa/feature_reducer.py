from rfa.utils import rf
from sklearn.model_selection import cross_val_score
from sklearn.metrics import make_scorer, matthews_corrcoef
import numpy as np
from tqdm.auto import tqdm


class FeatureReducer:
    """
    A class for reducing the number of features in a dataset based on feature importance and model performance.

    Attributes:
        model: Initial Random Forest model.
        train_data: Training dataset.
        train_labels: Training labels.
        valid_data: Validation dataset.
        valid_labels: Validation labels.
        feature_importances: DataFrame of feature importances.
        mcc_bound: Maximum allowable difference in the MCC score.
        cv_folds: Number of folds for cross-validation.
    """

    def __init__(
        self, mcc_bound=0.005, cv_folds=5, n_estimators=40, max_features=0.5, min_samples_leaf=5
    ):
        self.mcc_bound = mcc_bound
        self.cv_folds = cv_folds
        self.n_estimators = n_estimators
        self.max_features = max_features
        self.min_samples_leaf = min_samples_leaf

    def reduce_features(
        self,
        train_data,
        train_labels,
        valid_data,
        valid_labels,
        feature_importances,
        model=None,
    ):
        """
        Function that returns the most important features that maintain the required MCC score.

        Output:
        important_model: Random Forest model trained on the reduced dataset.
        important_train_data: Reduced training dataset.
        important_valid_data: Reduced validation dataset.
        kept_features: List of kept feature names.
        """

        if model is None:
            # Training initial model on all features
            model = rf(
                train_data,
                train_labels,
                n_estimators=self.n_estimators,
                max_features=self.max_features,
                min_samples_leaf=self.min_samples_leaf,
            )

        # Filter out features with zero importance
        nonzero_importances = feature_importances.loc[feature_importances["imp"] != 0]

        # Set threshold to keep features with importance above the 99th percentile
        threshold = 0.99
        kept_features = feature_importances[
            feature_importances.imp > nonzero_importances["imp"].quantile(0.99)
        ].features

        # Create reduced datasets using the features to keep
        important_train_data = train_data[kept_features]
        important_valid_data = valid_data[kept_features]

        # Train a new random forest model on the reduced dataset
        important_model = rf(
            important_train_data,
            train_labels,
            self.n_estimators,
            self.max_features,
            self.min_samples_leaf,
        )

        # Initialize the importance offset
        imp_offset = 0

        # Create a scorer function
        mcc_scorer = make_scorer(matthews_corrcoef)

        # Perform cross-validation on full model and get the mean MCC score
        mean_mcc_score = np.mean(
            cross_val_score(
                model, valid_data, valid_labels, cv=self.cv_folds, scoring=mcc_scorer
            )
        )

        # Keep features that maintain MCC score within the acceptable bounds
        for imp_offset in tqdm(range(100)):
            imp_offset /= 100  # Convert to a percentage
            if (
                mean_mcc_score
                - np.mean(
                    cross_val_score(
                        important_model,
                        important_valid_data,
                        valid_labels,
                        cv=self.cv_folds,
                        scoring=mcc_scorer,
                    )
                )
            ) > self.mcc_bound:
                if threshold - imp_offset == 0:
                    print("Could Not Reduce Feature Set")
                    return model, train_data, valid_data, list(train_data.columns)
                else:
                    (
                        imp_offset,
                        important_train_data,
                        important_valid_data,
                        important_model,
                        kept_features,
                    ) = self.update_model(
                        imp_offset,
                        feature_importances,
                        train_data,
                        train_labels,
                        valid_data,
                        nonzero_importances,
                    )
            else:
                return (
                    important_model,
                    important_train_data,
                    important_valid_data,
                    kept_features,
                )

    def update_model(
        self,
        imp_offset,
        feature_importances,
        train_data,
        train_labels,
        valid_data,
        nonzero_importances,
    ):
        """
        Updates the model with a new set of features.

        Input:
        imp_offset - Value used to adjust important feature search (float)
        feature_importances: DataFrame of feature importances.
        train_data: Training dataset.
        train_labels: Training labels.
        valid_data: Validation dataset.
        nonzero_importances: DataFrame of feature importances without zero importance features.

        Output:
        imp_offset: New offset value (float).
        important_train_data: Reduced training dataset.
        important_valid_data: Reduced validation dataset.
        important_model: Random Forest model trained on the reduced dataset.
        kept_features: List of kept feature names.
        """

        # Increase the importance offset
        imp_offset += 0.01

        # Update the list of features to keep based on the new threshold
        kept_features = feature_importances[
            feature_importances.imp
            > nonzero_importances["imp"].quantile(0.99 - imp_offset)
        ].features

        # Create reduced datasets using the updated list of features to keep
        important_train_data = train_data[kept_features]
        important_valid_data = valid_data[kept_features]

        # Train a new random forest model on the reduced dataset
        important_model = rf(
            important_train_data,
            train_labels,
            self.n_estimators,
            self.max_features,
            self.min_samples_leaf,
        )

        return (
            imp_offset,
            important_train_data,
            important_valid_data,
            important_model,
            kept_features,
        )
