import numpy as np
from scipy.stats import spearmanr
from scipy.cluster import hierarchy as hc
from scipy.cluster.hierarchy import fcluster
from rfa.utils import (
    nextnonexistent,
    get_average_feature_importance,
    get_average_feature_importance_parallel,
    save_fi,
)
from fastai.tabular.all import *


def preprocessing(df, dep_var, max_card=20, test_size=0.2, idx=None):
    """
    Preprocesses the dataframe by splitting it into training and validation sets, filling missing values,
    normalizing continuous variables, and categorifying categorical variables.

    Parameters:
    - df: The input pandas DataFrame.
    - max_card: Maximum cardinality of a numerical categorical variable.
    - dep_var: The name of the target variable in the DataFrame.
    - test_size: The proportion of the dataset to include in the test split.
    - idx: The indices for the train-test split. If None, a default split will be used.

    Returns:
    - xs, y: The feature matrix and target array for the training set.
    - valid_xs, valid_y: The feature matrix and target array for the validation set.
    """
    # Fastai's processing into training and validation groups
    cont_list, cat_list = cont_cat_split(df, max_card=max_card, dep_var=dep_var)

    if idx is None:
        splits = TrainTestSplitter(test_size=test_size, stratify=df[dep_var])(
            range_of(df)
        )
    else:
        splits = IndexSplitter(idx)(range_of(df))

    # Preprocess the DataFrame
    to = TabularPandas(
        df,
        procs=[FillMissing, Normalize, Categorify],
        cat_names=cat_list,
        cont_names=cont_list,
        y_names=dep_var,
        splits=splits,
    )

    xs, y = to.train.xs, to.train.y
    valid_xs, valid_y = to.valid.xs, to.valid.y

    return xs, y, valid_xs, valid_y


def get_feature_importance(
    xs,
    y,
    dep_var,
    n_estimators=40,
    max_features=0.5,
    min_samples_leaf=5,
    n_iterations=10,
    parallel=False,
    path=None,
):
    """
    Gets the feature importance from a RandomForest model trained on the data.
    It either runs in parallel or sequentially based on the 'parallel' flag.

    Parameters:
    - xs: The feature matrix for the training set.
    - y: The target array for the training set.
    - dep_var: The name of the target variable.
    - n_estimators: The number of trees in the random forest.
    - max_features: The number of features to consider when looking for the best split.
    - min_samples_leaf: The minimum number of samples required to be at a leaf node.
    - n_iterations: The number of iterations for feature importance calculation.
    - parallel: Whether to run feature importance calculation in parallel.
    - path: The path to the directory where the outputs will be saved. If None, nothing is saved.

    Returns:
    - fi: The feature importances.
    """
    if parallel:
        fi = get_average_feature_importance_parallel(
            xs,
            y,
            n_estimators,
            max_features,
            min_samples_leaf,
            n_iterations,
        )
    else:
        fi = get_average_feature_importance(
            xs,
            y,
            n_estimators,
            max_features,
            min_samples_leaf,
            n_iterations,
        )

    # Save feature importance as a csv
    if path is not None:
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)

        analysis_folder_path = os.path.join(path, dep_var)
        os.makedirs(analysis_folder_path, exist_ok=True)
        feature_importance_path = os.path.join(
            analysis_folder_path, "feature_importance.csv"
        )

        # Generate a new non-existing path for saving feature importance
        feature_importance_path = nextnonexistent(feature_importance_path)

        # Save the feature importances to a csv file
        fi.to_csv(feature_importance_path, index=False)

        # Save feature importance plot
        save_fi(fi, len(xs.columns), analysis_folder_path)

    return fi


def cluster_features(df, distance_threshold=0.01):
    """
    Function to find and remove redundant features based on Spearman correlation and hierarchical clustering.

    Parameters:
    df (pandas.DataFrame): The input dataframe.
    distance_threshold (float, optional): The distance threshold for forming clusters. Default is 0.01.

    Returns:
    to_drop (list): A list of column names that are suggested to be dropped.
    """

    # Calculate the Spearman correlation between columns in the dataframe
    corr = np.round(spearmanr(df).correlation, 4)

    # Convert the correlation matrix into a condensed form
    corr_condensed = hc.distance.squareform(1 - corr)

    # Perform hierarchical clustering on the condensed correlation matrix
    z = hc.linkage(corr_condensed, method="average")

    # Form clusters from the dendrogram using a distance threshold
    fl = fcluster(z, t=distance_threshold, criterion="distance")

    # Extract the column names of the dataframe
    col_list = list(df.columns)

    # Find and store redundant feature clusters
    clusters = []
    for cluster_ind in fl:
        cluster = np.where(fl == cluster_ind)[0].tolist()
        names = [col_list[ind] for ind in cluster]

        if len(names) > 1:
            clusters.append(names)

    # Remove duplicate clusters and flatten the list
    unique_data = list(map(list, set(map(tuple, clusters))))

    # Keep only the first feature of each cluster and mark the others for removal
    to_drop = [name for lst in unique_data for name in lst[1:]]

    return to_drop
