import pandas as pd
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
from tqdm.auto import tqdm
import time
import re
from joblib import Parallel, delayed
import umap


def rf(xs, y, n_estimators=40, max_features=0.5, min_samples_leaf=5, **kwargs):
    """Random Forest Classifier"""
    return RandomForestClassifier(
        n_jobs=-1,
        n_estimators=n_estimators,
        max_samples=None,
        class_weight="balanced",
        max_features=max_features,
        min_samples_leaf=min_samples_leaf,
        oob_score=True,
    ).fit(xs, y)

def rf_regressor(xs, y, n_estimators=40, max_features=0.5, min_samples_leaf=5, **kwargs):
    """Random Forest Classifier"""
    return RandomForestRegressor(
        n_jobs=-1,
        n_estimators=n_estimators,
        max_samples=None,
        max_features=max_features,
        min_samples_leaf=min_samples_leaf,
        oob_score=True,
    ).fit(xs, y)

def format_time(seconds):
    """Format time duration into a string with hours, minutes and seconds."""
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)

    if hours > 0:
        return f"{int(hours)}h {int(minutes)}m {int(seconds)}s"
    elif minutes > 0:
        return f"{int(minutes)}m {int(seconds)}s"
    else:
        return f"{int(seconds)}s"


def nextnonexistent(f):
    """Check Figure Existence for Naming Convention"""
    fnew = f
    root, ext = os.path.splitext(f)
    i = 0
    while os.path.exists(fnew):
        i += 1
        fnew = "%s_%i%s" % (root, i, ext)
    return fnew


def save_fi(fi, len_columns, path_to_folder):
    """Plotting Function for Feature Importance"""
    if len_columns < 30:
        top_feature_length = len_columns
    else:
        top_feature_length = 30

    fi[:top_feature_length].plot(
        "features", "imp", "barh", figsize=(12, 7), legend=False
    )

    title = f"Top {top_feature_length} Features"

    path = os.path.join(path_to_folder, f'{title.lower().replace(" ", "_")}.png')

    plt.title(title)
    plt.savefig(nextnonexistent(path))
    plt.close()


def save_confusion_matrices(model, X_test, y_test, labels, name, path_to_folder):
    # Calculate the non-normalized confusion matrix
    cm = confusion_matrix(y_test, model.predict(X_test))

    path = os.path.join(path_to_folder, f"{name}_non_normalized_cm.png")

    # Plot non-normalized confusion matrix
    plt.figure(figsize=(10, 7))
    sns.heatmap(cm, annot=True, fmt="d", xticklabels=labels, yticklabels=labels)
    plt.xlabel("Predicted")
    plt.ylabel("Actual")
    plt.title("Non-normalized Confusion Matrix")
    plt.savefig(nextnonexistent(path))
    plt.close()

    # Calculate the normalized confusion matrix
    cm_normalized = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]

    path = os.path.join(path_to_folder, f"{name}_normalized_cm.png")

    # Plot normalized confusion matrix
    plt.figure(figsize=(10, 7))
    sns.heatmap(
        cm_normalized,
        annot=True,
        fmt=".2f",
        xticklabels=labels,
        yticklabels=labels,
        cmap=plt.cm.Blues,
    )
    plt.xlabel("Predicted")
    plt.ylabel("Actual")
    plt.title("Normalized Confusion Matrix")
    plt.savefig(nextnonexistent(path))
    plt.close()


def get_feature_importance(X, y, n_estimators, max_features, min_samples_leaf):
    model = rf(X, y, n_estimators, max_features, min_samples_leaf)
    return model.feature_importances_


def get_average_feature_importance(
    X, y, n_estimators, max_features, min_samples_leaf, n_iterations
):
    # Initialize an array to hold the feature importances
    feature_importances = np.zeros(X.shape[1])

    for _ in tqdm(range(n_iterations)):
        # Add the feature importances to the total
        feature_importances += get_feature_importance(
            X, y, n_estimators, max_features, min_samples_leaf
        )

    # Divide by the number of iterations to get the average feature importance
    feature_importances /= n_iterations

    # Create a data frame
    feature_importances_df = pd.DataFrame(
        {"features": X.columns, "imp": feature_importances}
    ).sort_values("imp", ascending=False)

    return feature_importances_df


def get_average_feature_importance_parallel(
    X, y, n_estimators, max_features, min_samples_leaf, n_iterations, n_jobs=-1
):
    # Get the feature importances for each model in parallel
    feature_importances = Parallel(n_jobs=n_jobs)(
        delayed(get_feature_importance)(
            X, y, n_estimators, max_features, min_samples_leaf
        )
        for _ in tqdm(range(n_iterations))
    )

    # Calculate the average feature importance
    feature_importances = np.mean(feature_importances, axis=0)

    # Create a data frame
    feature_importances_df = pd.DataFrame(
        {"features": X.columns, "imp": feature_importances}
    ).sort_values("imp", ascending=False)

    return feature_importances_df


def draw_umap(n_neighbors, min_dist, n_components, data, verbose, metric="euclidean"):
    """UMAP Fitting Function"""
    time_start = time.time()
    fit = umap.UMAP(
        n_neighbors=n_neighbors,
        min_dist=min_dist,
        n_components=n_components,
        metric=metric,
        init="spectral",
        verbose=verbose,
    )
    u = fit.fit_transform(data)
    print(f"umap done! Time elapsed: {format_time(time.time()-time_start)}")
    return u


def generate_umap_2d_plot(
    dep_var,
    df_final,
    target_col,
    label_col,
    classnames,
    n,
    path_to_folder,
    verbose,
):
    # Generate UMAP results
    umap_result_2d = draw_umap(n, 0.1, 2, df_final, verbose)

    # Create a dataframe from UMAP results, target column, and label column
    umap_df_2d = pd.DataFrame(
        {"u1": umap_result_2d[:, 0], "u2": umap_result_2d[:, 1], dep_var: target_col}
    )
    umap_df_2d = umap_df_2d.join(label_col, rsuffix=" label")

    # Initialize figure
    plt.figure(figsize=(15, 15))

    # Generate scatterplot with seaborn
    sc = sns.scatterplot(
        x="u1",
        y="u2",
        hue=dep_var + " label",
        hue_order=classnames,
        palette=sns.color_palette("hls", len(classnames)),
        data=umap_df_2d,
        legend="full",
        alpha=1,
    )

    # Remove the default legend title
    sc.legend_.set_title(None)

    path = os.path.join(path_to_folder, "UMAP_2d.png")

    plt.title(f"UMAP 2D: n_neighbours={n}", fontdict={"fontsize": 24})
    plt.legend(loc="upper right", frameon=True, fontsize=20)
    plt.savefig(nextnonexistent(path))
    plt.close()


def generate_umap_3d_plot(
    dep_var,
    df_final,
    target_col,
    label_col,
    classnames,
    n,
    path_to_folder,
    verbose,
):
    # Generate UMAP results
    umap_result = draw_umap(n, 0.1, 3, df_final, verbose)

    # Create a dataframe from UMAP results, target column, and label column
    umap_df = pd.DataFrame(
        {
            "u1": umap_result[:, 0],
            "u2": umap_result[:, 1],
            "u3": umap_result[:, 2],
            dep_var: target_col,
        }
    )
    umap_df = umap_df.join(label_col, rsuffix=" label")

    # Create a 3D scatterplot
    fig = plt.figure(figsize=(15, 15))
    ax = fig.add_subplot(111, projection="3d")

    sc = ax.scatter(
        xs=umap_df["u1"],
        ys=umap_df["u2"],
        zs=umap_df["u3"],
        c=umap_df[dep_var],
        cmap="viridis",
        alpha=1,
        edgecolors="w",
        linewidth=0.8,
    )

    # Set labels for axes
    ax.set_xlabel("umap-one")
    ax.set_ylabel("umap-two")
    ax.set_zlabel("umap-three")

    # Adjust legend to display class names instead of numbers
    legend_elements = sc.legend_elements(alpha=1)
    true_class_names = [
        classnames[int(re.search(r"\{([A-Za-z0-9_]+)\}", entry).group(1))]
        for entry in legend_elements[1]
    ]

    ax.legend(
        handles=legend_elements[0],
        labels=true_class_names,
        loc="upper left",
        bbox_to_anchor=(1.05, 1),
        fontsize=16,
    )

    path = os.path.join(path_to_folder, "UMAP_3d.png")

    plt.tight_layout()
    plt.title(f"UMAP 3D: n_neighbours={n}", fontdict={"fontsize": 24})
    plt.savefig(nextnonexistent(path))
    plt.close()
