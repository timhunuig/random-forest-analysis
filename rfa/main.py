from rfa.feature_reducer import FeatureReducer
from rfa.utils import (
    rf,
    format_time,
    save_confusion_matrices,
    generate_umap_2d_plot,
    generate_umap_3d_plot,
)
from rfa.operations import preprocessing, get_feature_importance, cluster_features
from fastai.tabular.all import *
import os
import time
from datetime import datetime
import logging
from sklearn.metrics import precision_score, recall_score, accuracy_score


def random_forest_analysis(
    path: str,
    df: pd.DataFrame,
    dep_var: str,
    idx: Optional[List[int]] = None,
    n_iterations: int = 10,
    bound: float = 0.005,
    test_size: float = 0.2,
    cv_folds: int = 5,
    n_estimators: int = 40,
    max_features: float = 0.5,
    min_samples_leaf: int = 5,
    max_card: int = 20,
    distance_threshold: float = 0.01,
    parallel: bool = False,
    umap_op: bool = False,
    n: int = 250,
    verbose: bool = False,
) -> List[str]:
    """
    This function performs random forest analysis, involving data preprocessing,
    model training, feature importance calculation, feature selection, model evaluation,
    and visualization.

    Parameters:
    - path: The path to the directory where the outputs will be saved.
    - df: The input pandas DataFrame.
    - dep_var: The name of the target variable in the DataFrame.
    - idx: The indices for the train-test split. If None, a default split will be used.
    - n_iterations: The number of iterations for feature importance calculation.
    - bound: The MCC score bound for feature selection.
    - cv_folds: The number of cross-validation folds for feature selection.
    - n_estimators: The number of trees in the random forest.
    - max_features: The number of features to consider when looking for the best split.
    - min_samples_leaf: The minimum number of samples required to be at a leaf node.
    - max_card: Maximum cardinality of a numerical categorical variable.
    - distance_threshold: The distance threshold for feature clustering.
    - parallel: Whether to run feature importance calculation in parallel.
    - umap_op: Whether to generate UMAP plots.
    - n: The number of neighbors to consider for UMAP.
    - verbose: Whether to print progress messages.

    Returns:
    - to_keep: A list of the names of the important features.
    - to_keep_final: A list of the names of the final features
    """

    # Check for null values in the DataFrame
    if df.isnull().sum().sum() > 0:
        raise ValueError("Input DataFrame contains null values")

    if not os.path.isdir(path):
        os.makedirs(path, exist_ok=True)

    analysis_folder_path = os.path.join(path, dep_var)
    os.makedirs(analysis_folder_path, exist_ok=True)

    # Set up logging
    logging.basicConfig(
        filename=f"{analysis_folder_path}/metrics.log", level=logging.INFO
    )

    # Record datetime and the start of logging file
    now = datetime.now()
    dt = now.strftime("%d/%m/%Y %H:%M:%S")
    logging.info(dt)

    # Record start time
    start_time = time.time()

    if verbose:
        print(f"Number of features: {df.shape[1]}")
        print("Splitting data into training and validation sets...")

    logging.info(f"Number of Features: {df.shape[1]}")

    xs, y, valid_xs, valid_y = preprocessing(df, dep_var, max_card, test_size, idx)

    if verbose:
        print(
            f"Train-test split done! Time elapsed: {format_time(time.time() - start_time)}"
        )
        print("Training initial model...")

    # Training initial model on all features
    m = rf(
        xs,
        y,
        n_estimators=n_estimators,
        max_features=max_features,
        min_samples_leaf=min_samples_leaf,
    )

    if verbose:
        print(
            f"Initial Model training done! Time elapsed: {format_time(time.time() - start_time)}"
        )
        print("Generating confusing matrices...")

    classnames = list(df[dep_var].unique())
    classnames.sort()

    path_to_confusion_matrices = os.path.join(
        analysis_folder_path, "confusion_matrices"
    )
    os.makedirs(path_to_confusion_matrices, exist_ok=True)

    preds = m.predict(valid_xs)

    save_confusion_matrices(
        m, valid_xs, valid_y, classnames, "initial", path_to_confusion_matrices
    )

    accuracy, precision, recall = (
        round(accuracy_score(valid_y, preds), 2),
        round(precision_score(valid_y, preds, average="macro"), 2),
        round(recall_score(valid_y, preds, average="macro"), 2),
    )
    logging.info(f"Accuracy of Important Feature Model: {accuracy}")
    logging.info(f"Precision of Important Feature Model: {precision}")
    logging.info(f"Recall of Important Feature Model: {recall}")

    if verbose:
        print(
            f"Confusion matrices generated! Time elapsed: {format_time(time.time() - start_time)}"
        )
        print(f"Accuracy of important feature model: {accuracy}")
        print(f"Precision of important feature model: {precision}")
        print(f"Recall of important feature model: {recall}")
        print("Finding important features...")

    path_to_feature_importance = os.path.join(
        analysis_folder_path, "feature_importance"
    )
    os.makedirs(path_to_feature_importance, exist_ok=True)

    fi = get_feature_importance(
        xs,
        y,
        dep_var,
        n_estimators,
        max_features,
        min_samples_leaf,
        n_iterations,
        parallel,
        path,
    )

    if verbose:
        print("Generating model with important features...")

    feature_reduction = FeatureReducer(
        bound,
        cv_folds,
        n_estimators,
        max_features,
        min_samples_leaf,
    )
    m_imp, xs_imp, valid_xs_imp, to_keep = feature_reduction.reduce_features(
        xs, y, valid_xs, valid_y, fi, m
    )

    if verbose:
        print(
            f"Found a minimum set of important features that are within MCC score bound: {len(to_keep)}."
        )

    logging.info(f"Number of Features after Reduction: {len(to_keep)}")

    save_confusion_matrices(
        m_imp,
        valid_xs_imp,
        valid_y,
        classnames,
        "important",
        path_to_confusion_matrices,
    )

    preds = m_imp.predict(valid_xs_imp)
    accuracy, precision, recall = (
        round(accuracy_score(valid_y, preds), 2),
        round(precision_score(valid_y, preds, average="macro"), 2),
        round(recall_score(valid_y, preds, average="macro"), 2),
    )
    logging.info(f"Accuracy of Important Feature Model: {accuracy}")
    logging.info(f"Precision of Important Feature Model: {precision}")
    logging.info(f"Recall of Important Feature Model: {recall}")

    if verbose:
        print(f"Accuracy of important feature model: {accuracy}")
        print(f"Precision of important feature model: {precision}")
        print(f"Recall of important feature model: {recall}")
        print(
            f"Model generated with important features done! Time elapsed: {format_time(time.time() - start_time)}"
        )
        print("Checking for redundant features...")

    to_drop = cluster_features(xs_imp, distance_threshold)

    # Remove redundant reactions from to_keep
    to_keep_final = [x for x in to_keep if x not in to_drop]

    if verbose:
        print(
            f"Redundancy check done! Time elapsed: {format_time(time.time() - start_time)}"
        )
        print(f"Number of features reduced from {df.shape[1]} to {len(to_keep_final)}")
        print("Training final model...")

    xs_final = xs_imp.drop(to_drop, axis=1)
    valid_xs_final = valid_xs_imp.drop(to_drop, axis=1)

    # Training on final features
    m_final = rf(xs_final, y, n_estimators, max_features, min_samples_leaf)

    save_confusion_matrices(
        m_final,
        valid_xs_final,
        valid_y,
        classnames,
        "final",
        path_to_confusion_matrices,
    )

    logging.info(f"Number of Features after Redundancy Check: {len(to_keep_final)}")

    preds = m_final.predict(valid_xs_final)
    accuracy, precision, recall = (
        round(accuracy_score(valid_y, preds), 2),
        round(precision_score(valid_y, preds, average="macro"), 2),
        round(recall_score(valid_y, preds, average="macro"), 2),
    )
    logging.info(f"Accuracy of Final Feature Model: {accuracy}")
    logging.info(f"Precision of Final Feature Model: {precision}")
    logging.info(f"Recall of Final Feature Model: {recall}")

    if verbose:
        print(f"Accuracy of final feature model: {accuracy}")
        print(f"Precision of final feature model: {precision}")
        print(f"Recall of final feature model: {recall}")

    if umap_op:
        if verbose:
            print("Generating UMAP plots...")

        # Reconstructing initial dataframe (needed as dataframe has been processed with fastai)
        df_train = xs_final.merge(y, how="left", left_index=True, right_index=True)
        df_valid = valid_xs_final.merge(
            valid_y, how="left", left_index=True, right_index=True
        )

        frames = [df_train, df_valid]
        df_combined = pd.concat(frames)

        df_final = df_combined.drop(columns=[dep_var])
        target_col = df_combined[dep_var]
        label_col = df[dep_var]

        path_to_umap = os.path.join(analysis_folder_path, "UMAP")
        os.makedirs(path_to_umap, exist_ok=True)

        generate_umap_2d_plot(
            dep_var,
            df_final,
            target_col,
            label_col,
            classnames,
            n,
            path_to_umap,
            verbose,
        )
        generate_umap_3d_plot(
            dep_var,
            df_final,
            target_col,
            label_col,
            classnames,
            n,
            path_to_umap,
            verbose,
        )

    if verbose:
        print(
            f"Random forest analysis done! Time elapsed: {format_time(time.time() - start_time)}"
        )

    return to_keep, to_keep_final
