import pandas as pd
import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from rfa.main import random_forest_analysis

if __name__ == "__main__":
    test_files_dir = os.path.join(os.path.dirname(__file__), "test_files")
    file_path = os.path.join(test_files_dir, "reaction_abundance_body_site.parquet")

    df = pd.read_parquet(file_path)

    to_keep, to_keep_final = random_forest_analysis(
        "output", df, dep_var="Body site", parallel=False, umap_op=False, verbose=True
    )

    print(to_keep)
